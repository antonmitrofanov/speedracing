import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var goButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var recordsButton: UIButton!
    
    let animationDuration = 0.3
    let buttonCornerRadius = CGFloat(20)
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupButton(self.goButton)
        self.setupButton(self.settingsButton)
        self.setupButton(self.recordsButton)
    }
    
    @IBAction func goButtonPressed(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "GoViewController") as! GoViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func settingsButtonPressed(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func recordsButtonPressed(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "RecordsViewController") as! RecordsViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func setupButton(_ button: UIButton) {
        button.layer.cornerRadius = self.buttonCornerRadius
    }
    
}

