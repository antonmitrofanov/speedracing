import UIKit

class RecordsTableViewCell: UITableViewCell {

    @IBOutlet weak var cellNameLabel: UILabel!
    @IBOutlet weak var cellTimeLabel: UILabel!
    @IBOutlet weak var cellRecordLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupCell(cell: RecordsTableViewCell, record: Record) {
        cell.cellNameLabel.text = record.getName()
        cell.cellNameLabel.textColor = Colors.shared.getButtonTextColor()
        cell.cellTimeLabel.text = record.getTime()
        cell.cellTimeLabel.textColor = Colors.shared.getButtonTextColor()
        cell.cellRecordLabel.text = String(record.getScore())
        cell.cellRecordLabel.textColor = Colors.shared.getButtonTextColor()
    }

}
