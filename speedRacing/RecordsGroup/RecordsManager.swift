import Foundation


class RecordsManager {
    
    static let shared = RecordsManager()
    private init(){}
    
    private let defaults = UserDefaults.standard
    
    func getRecordsArray() -> [Record]? {
        guard let data = self.defaults.value(forKey: Keys.recordsArray.rawValue) as? Data else {
            return nil
        }
        let recordsArray = try? JSONDecoder().decode([Record].self, from: data)
        if var array = recordsArray {
            array.sort {$0.getScore() > $1.getScore()}
            return array
        } else {
            return nil
        }
    }
    
    func saveRecordsArray(recordsArray: [Record]) {
        if let data = try? JSONEncoder().encode(recordsArray) {
            self.defaults.set(data, forKey: Keys.recordsArray.rawValue)
            self.defaults.synchronize()
        }
    }
    
    func addNewRecord(_ record: Record) {
        if var recordsArray = self.getRecordsArray() {
            recordsArray.append(record)
            self.saveRecordsArray(recordsArray: recordsArray)
        } else {
            var recordsArray: [Record] = []
            recordsArray.append(record)
            self.saveRecordsArray(recordsArray: recordsArray)
        }
    }
    
    func deleteRecord(index: Int) {
        if var recordsArray = self.getRecordsArray() {
            recordsArray.remove(at: index)
            self.saveRecordsArray(recordsArray: recordsArray)
        }
    }
    
}
