import UIKit

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var redCarImageView: UIImageView!
    @IBOutlet weak var yellowCarImageView: UIImageView!
    @IBOutlet weak var redCarSwitch: UISwitch!
    @IBOutlet weak var yellowCarSwitch: UISwitch!
    @IBOutlet weak var cactusImageView: UIImageView!
    @IBOutlet weak var vanImageView: UIImageView!
    @IBOutlet weak var rockImageView: UIImageView!
    @IBOutlet weak var cactusSwitch: UISwitch!
    @IBOutlet weak var vanSwitch: UISwitch!
    @IBOutlet weak var rockSwitch: UISwitch!
    
    override func viewWillAppear(_ animated: Bool) {
        if let carArray = SettingsManager.shared.getCarArray() {
            if let image = UIImage(named: "redCar") {
                if carArray.contains(image) {
                    self.redCarSwitch.isOn = true
                } else {
                    self.redCarSwitch.isOn = false
                }
            }
            if let image = UIImage(named: "yellowCar") {
                if carArray.contains(image) {
                    self.yellowCarSwitch.isOn = true
                } else {
                    self.yellowCarSwitch.isOn = false
                }
            }
        }
        
        if let resistArray = SettingsManager.shared.getResistArray() {
            if let image = UIImage(named: "cactus") {
                if resistArray.contains(image) {
                    self.cactusSwitch.isOn = true
                } else {
                    self.cactusSwitch.isOn = false
                }
            }
            if let image = UIImage(named: "van") {
                if resistArray.contains(image) {
                    self.vanSwitch.isOn = true
                } else {
                    self.vanSwitch.isOn = false
                }
            }
            if let image = UIImage(named: "rock") {
                if resistArray.contains(image) {
                    self.rockSwitch.isOn = true
                } else {
                    self.rockSwitch.isOn = false
                }
            }
        }
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func redCarSwitchValueChanged(_ sender: UISwitch) {
        if redCarSwitch.isOn {
            if let image = UIImage(named: "redCar") {
                SettingsManager.shared.appendCarArray(image: image)
            }
        } else if !redCarSwitch.isOn && !yellowCarSwitch.isOn {
            if let image = UIImage(named: "yellowCar") {
                if let image = UIImage(named: "redCar") {
                    SettingsManager.shared.removeCar(image: image)
                }
                SettingsManager.shared.appendCarArray(image: image)
                self.yellowCarSwitch.isOn = true
            }
        } else if !redCarSwitch.isOn {
            if let image = UIImage(named: "redCar") {
                SettingsManager.shared.removeCar(image: image)
            }
        }
    }
    
    @IBAction func yellowCarSwitchValueChanged(_ sender: UISwitch) {
        if yellowCarSwitch.isOn {
            if let image = UIImage(named: "yellowCar") {
                SettingsManager.shared.appendCarArray(image: image)
            }
        } else if !yellowCarSwitch.isOn && !redCarSwitch.isOn {
            if let image = UIImage(named: "redCar") {
                if let image = UIImage(named: "yellowCar") {
                    SettingsManager.shared.removeCar(image: image)
                }
                SettingsManager.shared.appendCarArray(image: image)
                self.redCarSwitch.isOn = true
            }
        } else if !yellowCarSwitch.isOn {
            if let image = UIImage(named: "yellowCar") {
                SettingsManager.shared.removeCar(image: image)
            }
        }
    }
    
    @IBAction func cactusSwitchVlaueChanged(_ sender: UISwitch) {
        if cactusSwitch.isOn {
            if let image = UIImage(named: "cactus") {
                SettingsManager.shared.appendResistArray(image: image)
            }
        } else if !cactusSwitch.isOn && !vanSwitch.isOn && !rockSwitch.isOn {
            cactusSwitch.isOn = true
        } else if !cactusSwitch.isOn {
            if let image = UIImage(named: "cactus") {
                SettingsManager.shared.removeResist(image: image)
            }
        }
    }
    
    @IBAction func vanSwitchValueChanged(_ sender: UISwitch) {
        if vanSwitch.isOn {
            if let image = UIImage(named: "van") {
                SettingsManager.shared.appendResistArray(image: image)
            }
        } else if !cactusSwitch.isOn && !vanSwitch.isOn && !rockSwitch.isOn {
            vanSwitch.isOn = true
        } else if !vanSwitch.isOn {
            if let image = UIImage(named: "van") {
                SettingsManager.shared.removeResist(image: image)
            }
        }
    }
    
    @IBAction func rockSwitchValueChanged(_ sender: UISwitch) {
        if rockSwitch.isOn {
            if let image = UIImage(named: "rock") {
                SettingsManager.shared.appendResistArray(image: image)
            }
        } else if !cactusSwitch.isOn && !vanSwitch.isOn && !rockSwitch.isOn {
            rockSwitch.isOn = true
        } else if !rockSwitch.isOn {
            if let image = UIImage(named: "rock") {
                SettingsManager.shared.removeResist(image: image)
            }
        }
    }
}
