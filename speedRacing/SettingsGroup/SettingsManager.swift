import Foundation
import UIKit

class SettingsManager {
    
    static let shared = SettingsManager()
    
    private init(){}
    
    private var carArray = [UIImage(named: "redCar"), UIImage(named: "yellowCar")]
    
    private var resistArray = [UIImage(named: "cactus"), UIImage(named: "van"), UIImage(named: "rock")]
    
    func getCarArray() -> [UIImage]? {
        if let carArray = self.carArray as? [UIImage] {
            return carArray
        } else {
            return nil
        }
    }
    
    func getResistArray() -> [UIImage]? {
        if let resistArray = self.resistArray as? [UIImage] {
            return resistArray
        } else {
            return nil
        }
    }
    
    func appendCarArray(image: UIImage) {
        self.carArray.append(image)
    }
    
    func removeCar(image: UIImage) {
        if self.carArray.contains(image) {
            let index = self.carArray.firstIndex(of: image)
            if let index = index {
                self.carArray.remove(at: index)
            }
        }
    }
    
    func appendResistArray(image: UIImage) {
        self.resistArray.append(image)
    }
    
    func removeResist(image: UIImage) {
        if self.resistArray.contains(image) {
            let index = self.resistArray.firstIndex(of: image)
            if let index = index {
                self.resistArray.remove(at: index)
            }
        }
    }
    
}
