import Foundation

enum Direction {
    case right
    case left
}

enum Keys: String {
    case recordsArray = "recordsArray"
}

