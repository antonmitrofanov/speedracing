import UIKit

class Colors {
    
    static let shared = Colors()
    
    private init(){}
    
    private let buttonTextColor = UIColor(r: 231, g: 154, b: 77)
    
    private let backgroundDefaultColor = UIColor(r: 136, g: 38, b: 80)
    
    private let navBarDefaultColor = UIColor(r: 105, g: 21, b: 59)
    
    func getButtonTextColor() -> UIColor {
        return self.buttonTextColor
    }
    
    func getBackgroundDefaultColor() -> UIColor {
        return self.backgroundDefaultColor
    }
    
    func getNavBarDefaultColor() -> UIColor {
        return self.navBarDefaultColor
    }
    
}
