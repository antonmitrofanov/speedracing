import UIKit

class GoViewController: UIViewController {
    
    // MARK:- IBOutlets

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var road: UIView!
    @IBOutlet weak var rightRoadside: UIView!
    @IBOutlet weak var leftRoadside: UIView!
    @IBOutlet weak var navigationBar: UIView!
    @IBOutlet weak var coverView: UIView!
    @IBOutlet weak var crashAlert: UIView!
    @IBOutlet weak var crashAlertTextField: UITextField!
    @IBOutlet weak var crashAlertSaveButton: UIButton!
    @IBOutlet weak var scoreLabel: UILabel!
    
    // MARK:- lets and vars
    
    let carStep = CGFloat(50)
    let carAnimationDuration = 0.2
    let carWidth = CGFloat(59)
    let carHeight = CGFloat(120)
    let carViewDistance = CGFloat(50)
    let car = UIImageView()
    
    let roadLineWidth = CGFloat(5)
    let roadLineHeight = CGFloat(25)
    let roadAnimationDurationStep = 0.1
    var roadAnimationDuration = 5.0
    
    let timerInterval = 1.0
    var timer = Timer()
    
    let bushTimerInterval = 2.0
    var bushTimer = Timer()
    
    let minBushDistance = 44
    let maxBushDistance = 440
    
    let crashAlertCornerRadius: CGFloat = 20
    
    let minResistDistance = 80
    let maxResistDistance = 800
    let resistTimerInterval = 2.7
    var resistTimer = Timer()
    
    var score: Int = 0
    var finalScore = 0
    
    var startTime: TimeInterval?
    var finishTime: TimeInterval?
    
    // MARK:- override funcs
    
    override func viewWillAppear(_ animated: Bool) {
        self.startTime = NSDate().timeIntervalSince1970
        self.createRecognizers()
        self.coverView.isHidden = true
        self.setupCrashAlert()
        self.setupTimer()
        self.setupBushTimer()
        if let carArray = SettingsManager.shared.getCarArray() {
            if carArray.count > 0 {
                let randomCarIndex = Int.random(in:0 ... (carArray.count-1))
                self.setupCar(image: carArray[randomCarIndex])
            }
        } else {
            self.navigationController?.popViewController(animated: true)
        }
        self.setupResistTimer()
    }
    
    // MARK:- IBActions
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func leftSwipeRecognized(_ sender: UISwipeGestureRecognizer) {
        self.moveCar(.left)
    }
    
    @IBAction func rightSwipeRecognized(_ sender: UISwipeGestureRecognizer) {
        self.moveCar(.right)
    }
    
    @IBAction func crashAlertSaveButtonPressed(_ sender: UIButton) {
        if let name = crashAlertTextField.text {
            let trimmedName = name.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
            if trimmedName != "" {
                guard let startTime = self.startTime, let finishTime = self.finishTime else {
                    return
                }
                let timeInterval = finishTime - startTime
                let mins = Int(round(timeInterval/60))
                let secs = Int(round(timeInterval - Double(mins) * 60))
                RecordsManager.shared.addNewRecord(Record(name: trimmedName, score: self.finalScore, time: String(mins) + "m" + " : " + String(secs) + "s"))
            }
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK:- everything connected with car
    
    func moveCar(_ direction: Direction) {
        switch direction {
        case .right:
            UIImageView.animate(withDuration: self.carAnimationDuration, animations: {
                self.car.frame.origin.x += self.carStep
            }) { (_) in
                if self.car.frame.origin.x >= self.road.frame.width + self.leftRoadside.frame.width - self.car.frame.width {
                    self.crash()
                }
            }
        case .left:
            UIImageView.animate(withDuration: self.carAnimationDuration, animations: {
                self.car.frame.origin.x -= self.carStep
            }) { (_) in
                if self.car.frame.origin.x <= self.road.frame.origin.x {
                    self.crash()
                }
            }
        }
    }
    
    func setupCar(image: UIImage) {
        self.car.frame = CGRect(x: self.view.frame.width/2 - self.carWidth/2, y: self.view.frame.height - self.carViewDistance - self.carHeight, width: self.carWidth, height: self.carHeight)
        self.car.image = image
        self.car.contentMode = .scaleAspectFit
        self.car.layer.borderWidth = 1
        self.car.layer.borderColor = UIColor.green.cgColor
        self.view.addSubview(self.car)
    }
    
    func crash() {
        self.finalScore = self.score
        if let image = UIImage(named: "crash") {
            self.car.image = image
        }
//        UIImageView.animate(withDuration: self.roadAnimationDuration, animations: {
//            self.car.frame.origin.y += self.view.frame.height - self.backButton.frame.height - self.carViewDistance + self.car.frame.height
//        })
        UIImageView.animate(withDuration: self.roadAnimationDuration, delay: 0.0, options: .curveLinear, animations: {
            self.car.frame.origin.y += self.view.frame.height - self.backButton.frame.height - self.carViewDistance + self.car.frame.height
        }, completion: nil)
        self.finishTime = NSDate().timeIntervalSince1970
        self.coverView.isHidden = false
        self.timer.invalidate()
        self.resistTimer.invalidate()
    }
    
    // MARK:- create and setup funcs
    
    func createRoadLines() {
        let roadLine = UIView()
        roadLine.frame = CGRect(x: self.view.frame.width/2 - self.roadLineWidth/2, y: -self.roadLineHeight, width: self.roadLineWidth, height: self.roadLineHeight)
        roadLine.backgroundColor = .yellow
        self.view.insertSubview(roadLine, belowSubview: self.navigationBar)
//        UIView.animate(withDuration: self.roadAnimationDuration, animations: {
//            roadLine.frame.origin.y += self.view.frame.height + roadLine.frame.height
//        }) { (_) in
//            roadLine.removeFromSuperview()
//        }
        UIView.animate(withDuration: self.roadAnimationDuration, delay: 0.0, options: .curveLinear, animations: {
            roadLine.frame.origin.y += self.view.frame.height + roadLine.frame.height
        }) { (_) in
            roadLine.removeFromSuperview()
        }
    }
    
    func createBushes(direction: Direction) {
        let randDistance = Int.random(in: self.minBushDistance ... self.maxBushDistance)
        let bush = UIImageView()
        switch direction {
        case .left:
            bush.frame = CGRect(x: self.view.frame.origin.x, y: -CGFloat(randDistance), width: self.leftRoadside.frame.width, height: self.leftRoadside.frame.width)
        case .right:
            bush.frame = CGRect(x: self.view.frame.width - self.rightRoadside.frame.width, y: -CGFloat(randDistance), width: self.rightRoadside.frame.width, height: self.rightRoadside.frame.width)
        }
        bush.image = UIImage(named: "bush")
        self.view.insertSubview(bush, belowSubview: self.navigationBar)
//        UIImageView.animate(withDuration: self.roadAnimationDuration, animations: {
//            bush.frame.origin.y += self.view.frame.height + bush.frame.height + CGFloat(randDistance)
//        }) { (_) in
//            bush.removeFromSuperview()
//        }
        UIImageView.animate(withDuration: self.roadAnimationDuration, delay: 0.0, options: .curveLinear, animations: {
            bush.frame.origin.y += self.view.frame.height + bush.frame.height + CGFloat(randDistance)
        }) { (_) in
            bush.removeFromSuperview()
        }
    }
    
    func createResist(resistArray: [UIImage]) {
        let randomResistDistance = Int.random(in: self.minResistDistance ... self.maxResistDistance)
        let randomIndex = Int.random(in: 0 ... resistArray.count-1)
        let image = resistArray[randomIndex]
        let resistWidth = self.road.frame.width/2
        let ratio = image.size.height/image.size.width
        let resistHeight = resistWidth * ratio
        let randomSide = Int.random(in: 1 ... 2)
        let resist = UIImageView()
        switch randomSide {
        case 1:
            resist.frame = CGRect(x: self.view.frame.origin.x + self.leftRoadside.frame.width, y: -CGFloat(randomResistDistance) - resistHeight, width: resistWidth, height: resistHeight)
        case 2:
            resist.frame = CGRect(x: self.view.frame.width/2, y: -CGFloat(randomResistDistance) - resistHeight, width: resistWidth, height: resistHeight)
        default:
            return
        }
        resist.image = image
        resist.layer.borderWidth = 1
        resist.layer.borderColor = UIColor.red.cgColor
        let intersectionTimerInterval = 0.05
        let intersectionTimer = Timer.scheduledTimer(withTimeInterval: intersectionTimerInterval, repeats: true) { (_) in
            if let frame = resist.layer.presentation()?.frame {
                if frame.intersects(self.car.frame) {
                    self.crash()
                }
            }
        }
        self.view.insertSubview(resist, belowSubview: self.navigationBar)
        intersectionTimer.fire()
//        UIImageView.animate(withDuration: self.roadAnimationDuration, animations: {
//            resist.frame.origin.y += CGFloat(randomResistDistance) + self.view.frame.height + resist.frame.height
//        }) { (_) in
//            resist.removeFromSuperview()
//            intersectionTimer.invalidate()
//            self.score += 1
//            self.scoreLabel.text = String(self.score)
//        }
        UIImageView.animate(withDuration: self.roadAnimationDuration, delay: 0.0, options: .curveLinear, animations: {
            resist.frame.origin.y += CGFloat(randomResistDistance) + self.view.frame.height + resist.frame.height
        }) { (_) in
            resist.removeFromSuperview()
            intersectionTimer.invalidate()
            self.score += 1
            self.scoreLabel.text = String(self.score)
        }
        self.roadAnimationDuration -= self.roadAnimationDurationStep
    }
    
    func createRecognizers() {
        let leftSwipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.leftSwipeRecognized(_:)))
        let rightSwipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.rightSwipeRecognized(_:)))
        leftSwipeRecognizer.direction = .left
        rightSwipeRecognizer.direction = .right
        self.view.addGestureRecognizer(leftSwipeRecognizer)
        self.view.addGestureRecognizer(rightSwipeRecognizer)
    }
    
    func setupTimer() {
        self.timer = Timer.scheduledTimer(withTimeInterval: self.timerInterval, repeats: true, block: { (_) in
            self.createRoadLines()
        })
        self.timer.fire()
    }
    
    func setupBushTimer() {
        self.bushTimer = Timer.scheduledTimer(withTimeInterval: self.bushTimerInterval, repeats: true, block: { (_) in
            self.createBushes(direction: .left)
            self.createBushes(direction: .right)
        })
        self.bushTimer.fire()
    }
    
    func setupResistTimer() {
        self.resistTimer = Timer.scheduledTimer(withTimeInterval: self.resistTimerInterval, repeats: true, block: { (_) in
            guard let resistArray = SettingsManager.shared.getResistArray() else {
                return
            }
            self.createResist(resistArray: resistArray)
        })
        self.resistTimer.fire()
    }
    
    func setupCrashAlert() {
        self.crashAlert.layer.cornerRadius = self.crashAlertCornerRadius
        self.crashAlertSaveButton.layer.cornerRadius = self.crashAlertCornerRadius
    }
    
}
